# infrastructure-management

#### 介绍
openKylin Infrastructure SIG负责openKylin社区的基础平台系统功能的开发、维护。

包括：
- OKBS: 软件包编译与仓库发布
- OKCI: 持续集成
- OKIF: 镜像生成
- CLA: CLA签署
- Weblate: 协作翻译

如果您使用openKylin的研发平台遇到任何问题，或者有什么改进建议，请提交issue。